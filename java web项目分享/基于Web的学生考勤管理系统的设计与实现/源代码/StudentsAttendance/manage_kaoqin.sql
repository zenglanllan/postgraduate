/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : manage_kaoqin

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-05-11 20:15:16
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `t_banji`
-- ----------------------------
DROP TABLE IF EXISTS `t_banji`;
CREATE TABLE `t_banji` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banjiname` varchar(255) DEFAULT NULL,
  `banjinum` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `yuanxi` varchar(255) DEFAULT NULL,
  `zhuanye` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_banji
-- ----------------------------
INSERT INTO `t_banji` VALUES ('1', '软件1305班', '10001', '2013-10-08 08:43:03', '软件学院', '软件工程');
INSERT INTO `t_banji` VALUES ('2', '软件1306班', '10002', '2013-10-08 08:43:03', '软件学院', '软件工程');
INSERT INTO `t_banji` VALUES ('3', '软件1307班', '10003', '2017-05-11 03:39:45', '软件学院', '软件工程');

-- ----------------------------
-- Table structure for `t_kaoqinlog`
-- ----------------------------
DROP TABLE IF EXISTS `t_kaoqinlog`;
CREATE TABLE `t_kaoqinlog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banjinum` varchar(255) DEFAULT NULL,
  `codenum` varchar(255) DEFAULT NULL,
  `createtime` time DEFAULT NULL,
  `iskuangke` int(11) NOT NULL DEFAULT '0',
  `kechengname` varchar(255) DEFAULT NULL,
  `kuangkenum` int(11) NOT NULL DEFAULT '0',
  `riqi` varchar(255) DEFAULT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `kqtype` varchar(255) NOT NULL DEFAULT '正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kaoqinlog
-- ----------------------------
INSERT INTO `t_kaoqinlog` VALUES ('1', '10001', 'S2010001', '10:39:28', '0', '软件工程', '0', '2015-02-15', 'T2010002', '正常');
INSERT INTO `t_kaoqinlog` VALUES ('4', '10001', 'S2010001', '14:33:18', '0', '离散数学', '0', '2015-02-16', 'T001', '迟到');
INSERT INTO `t_kaoqinlog` VALUES ('5', '10001', 'S2010001', '16:41:37', '0', '高等数学', '0', '2015-02-16', 'T001', '正常');
INSERT INTO `t_kaoqinlog` VALUES ('6', '10001', 'S2010001', '16:54:14', '0', 'C程序设计', '0', '2015-02-16', 'T001', '迟到');
INSERT INTO `t_kaoqinlog` VALUES ('7', '10001', 'S2010001', '17:01:50', '0', 'C程序设计', '0', '2015-02-16', 'T001', '正常');
INSERT INTO `t_kaoqinlog` VALUES ('8', '10001', 'S2010001', '17:18:08', '0', 'Java程序设计', '0', '2015-02-16', 'T001', '迟到');
INSERT INTO `t_kaoqinlog` VALUES ('9', '10001', 'S2010001', '17:41:31', '0', '高等数学', '0', '2015-02-16', 'T001', '迟到');
INSERT INTO `t_kaoqinlog` VALUES ('10', '10001', 'S2010001', '17:55:10', '0', '高等数学', '0', '2015-02-16', 'T001', '正常');
INSERT INTO `t_kaoqinlog` VALUES ('11', '10001', 'S2010001', '18:00:09', '0', '软件工程', '0', '2015-02-16', 'T001', '迟到');
INSERT INTO `t_kaoqinlog` VALUES ('12', '10001', '2013005544', '03:41:05', '0', '高等数学', '0', '2017-05-11', 'MISS JIN', '正常');

-- ----------------------------
-- Table structure for `t_kecheng`
-- ----------------------------
DROP TABLE IF EXISTS `t_kecheng`;
CREATE TABLE `t_kecheng` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banji` varchar(255) DEFAULT NULL,
  `banjinum` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `kechenglock` int(11) DEFAULT NULL,
  `kechengname1` varchar(255) DEFAULT NULL,
  `kechengname2` varchar(255) DEFAULT NULL,
  `kechengname3` varchar(255) DEFAULT NULL,
  `kechengname4` varchar(255) DEFAULT NULL,
  `kechengname5` varchar(255) DEFAULT NULL,
  `kechengname6` varchar(255) DEFAULT NULL,
  `kechengname7` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_kecheng
-- ----------------------------
INSERT INTO `t_kecheng` VALUES ('1', null, '10001', '2015-02-15 18:47:59', '1', '计算机导论', 'C程序设计', '计算机图形学', 'Java程序设计', 'Web开发', '乒乓球技巧', '大学俄语');
INSERT INTO `t_kecheng` VALUES ('2', null, '10002', '2015-02-15 18:49:59', '3', '高等数学', '线性代数', '微分方程', 'VC++程序设计', 'ASP.NET程序设计', '艺术', '古典音乐');
INSERT INTO `t_kecheng` VALUES ('3', null, '10001', '2017-05-11 03:38:31', '10', '数据结构', '操作系统', '大学英语', '高等数学', '线性代数', '离散结构', '软件工程导论');

-- ----------------------------
-- Table structure for `t_qingjiadan`
-- ----------------------------
DROP TABLE IF EXISTS `t_qingjiadan`;
CREATE TABLE `t_qingjiadan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codenum` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `banjinum` varchar(255) DEFAULT NULL,
  `shenhe` varchar(255) DEFAULT NULL,
  `shenhecontent` varchar(255) DEFAULT NULL,
  `qjcontent` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `qjtime1` varchar(255) DEFAULT NULL,
  `qjtime2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_qingjiadan
-- ----------------------------
INSERT INTO `t_qingjiadan` VALUES ('3', 'S2010001', '庞晓磊', '10001', '审核中', null, 'happy', '2015-02-15 12:39:21', '2015-02-15 00:00:00', '2015-02-17 00:00:00');
INSERT INTO `t_qingjiadan` VALUES ('4', 'S2010001', '任志敏', '10001', '审核中', null, '有考试', '2015-02-15 13:07:56', '2015-02-16 13:07:38', '2015-02-17 13:07:42');
INSERT INTO `t_qingjiadan` VALUES ('5', '2013005544', '庞晓磊', '10001', '通过', '', '答辩', '2017-05-11 03:41:32', '2017-05-02 03:41:19', '2017-05-27 03:41:23');

-- ----------------------------
-- Table structure for `t_subject`
-- ----------------------------
DROP TABLE IF EXISTS `t_subject`;
CREATE TABLE `t_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectname` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `sksj` time DEFAULT NULL,
  `xksj` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_subject
-- ----------------------------
INSERT INTO `t_subject` VALUES ('1', '高等数学', '2015-02-15 18:37:40', '08:00:00', '12:00:00');
INSERT INTO `t_subject` VALUES ('2', '软件工程', '2015-02-15 18:38:06', '09:30:00', '11:30:00');
INSERT INTO `t_subject` VALUES ('3', 'C程序设计', '2015-02-15 18:41:30', '13:30:00', '15:30:00');
INSERT INTO `t_subject` VALUES ('4', 'Java程序设计', '2015-02-15 13:07:12', '10:00:00', '12:00:00');
INSERT INTO `t_subject` VALUES ('5', '离散数学', '2015-02-16 11:45:52', '08:00:00', '10:00:00');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `banjinum` varchar(255) DEFAULT NULL,
  `codenum` varchar(255) DEFAULT NULL,
  `createtime` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `room` varchar(255) DEFAULT NULL,
  `userlock` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'CHINA', '0', 'admin', '2015-02-15 00:00:00', '123456', '888888', '0', '0', '0', 'admin');
INSERT INTO `t_user` VALUES ('2', 'CHINA,BEIJING', '10001', '2013005544', '2015-02-15 18:54:35', '123456', '13555555555', '4', '202', '0', '庞晓磊');
INSERT INTO `t_user` VALUES ('3', '太原市迎泽西大街', '10001', 'T2010002', '2015-02-15 19:03:43', '123456', '13888899999', '2', '', '0', '001');
INSERT INTO `t_user` VALUES ('4', '太原市迎泽西大街79号', '10001', 'B2010001', '2015-02-15 19:05:14', '123456', '13666666666', '3', '', '0', 'MISS ZHANG');
INSERT INTO `t_user` VALUES ('5', '太原市迎泽西大街79号', '', '001', '2015-02-15 19:06:29', '123456', '13999999999', '1', '', '0', 'MISS JIN');
