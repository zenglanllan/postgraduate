package com.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.dao.TUserDAO;
import com.model.TUser;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class userAction extends ActionSupport
{
	private Integer id;
	private String loginname;
	private String loginpw;
	private String name;

	private String sex;
	private String age;
	private String address;
	private String xueli;
	
	private String dianhua;
	
	private String del;
	
	private String message;
	private String path;
	
	private TUserDAO userDAO;
	
	
	public String userReg()
	{
        HttpServletRequest request=ServletActionContext.getRequest();
		
		String sql="from TUser where loginname=? and del='no'";
		Object[] c={loginname.trim()};
		List list=userDAO.getHibernateTemplate().find(sql,c);
		if(list.size()>0)
		{
			request.setAttribute("msg", "用户名已被占用,请重新注册");
		}
		else
		{
			TUser user=new TUser();
			
			user.setLoginname(loginname);
			user.setLoginpw(loginpw);
			user.setName(name);
			user.setSex(sex);
			
			user.setAge(age);
			user.setAddress(address);
			user.setXueli(xueli);
			user.setDianhua(dianhua);
			
			user.setDel("no");
			
			userDAO.save(user);
			
			request.setAttribute("msg", "注册成功，请登录");
		}
		
		return "msg";
	}
	
	
	public String userEdit()
	{
        HttpServletRequest request=ServletActionContext.getRequest();
        Map session=ActionContext.getContext().getSession();
		 
        TUser user=(TUser)session.get("user");
		
			
        user.setLoginname(loginname);
		user.setLoginpw(loginpw);
		user.setName(name);
		user.setSex(sex);
		
		user.setAge(age);
		user.setAddress(address);
		user.setXueli(xueli);
		user.setDianhua(dianhua);
		
		user.setDel("no");
		
		userDAO.attachDirty(user);
		
		request.setAttribute("msg", "修改成功，重新登了后生效");
		
		
		return "msg";
	}
	
	
	
	public String userMana()
	{
		String sql="from TUser where del='no'";
		List userList=userDAO.getHibernateTemplate().find(sql);
		Map request=(Map)ServletActionContext.getContext().get("request");
		request.put("userList", userList);
		return ActionSupport.SUCCESS;
	}
	
	public String userDel()
	{
		TUser user=userDAO.findById(id);
		user.setDel("yes");
		userDAO.attachDirty(user);
		this.setMessage("删除成功");
		this.setPath("userMana.action");
		return "succeed";
	}
	
	
	
	public String userRes()
	{
		StringBuffer sql=new StringBuffer("from TUser where del='no' and 1=1");
		sql.append(" and name like '%"+name.trim()+"%'");
		sql.append(" and sex like '%"+sex.trim()+"%'");
		sql.append(" and xueli like '%"+xueli.trim()+"%'");
		
		List userList=userDAO.getHibernateTemplate().find(sql.toString());
		Map request=(Map)ServletActionContext.getContext().get("request");
		request.put("userList", userList);
		return ActionSupport.SUCCESS;
	}

	public String userDetail()
	{
		TUser user=userDAO.findById(id);
		Map request=(Map)ServletActionContext.getContext().get("request");
		request.put("user", user);
		return ActionSupport.SUCCESS;
	}


	public String getAddress()
	{
		return address;
	}



	public void setAddress(String address)
	{
		this.address = address;
	}



	public String getAge()
	{
		return age;
	}



	public String getXueli() {
		return xueli;
	}


	public void setXueli(String xueli) {
		this.xueli = xueli;
	}


	public String getDianhua() {
		return dianhua;
	}


	public void setDianhua(String dianhua) {
		this.dianhua = dianhua;
	}



	public String getDel() {
		return del;
	}


	public void setDel(String del) {
		this.del = del;
	}


	public void setAge(String age)
	{
		this.age = age;
	}



	public Integer getId()
	{
		return id;
	}



	public void setId(Integer id)
	{
		this.id = id;
	}



	public String getLoginname()
	{
		return loginname;
	}



	public void setLoginname(String loginname)
	{
		this.loginname = loginname;
	}



	public String getLoginpw()
	{
		return loginpw;
	}



	public void setLoginpw(String loginpw)
	{
		this.loginpw = loginpw;
	}



	public String getMessage()
	{
		return message;
	}



	public void setMessage(String message)
	{
		this.message = message;
	}



	public String getName()
	{
		return name;
	}



	public void setName(String name)
	{
		this.name = name;
	}



	public String getPath()
	{
		return path;
	}



	public void setPath(String path)
	{
		this.path = path;
	}



	public String getSex()
	{
		return sex;
	}



	public void setSex(String sex)
	{
		this.sex = sex;
	}



	public TUserDAO getUserDAO()
	{
		return userDAO;
	}



	public void setUserDAO(TUserDAO userDAO)
	{
		this.userDAO = userDAO;
	}
	
}
