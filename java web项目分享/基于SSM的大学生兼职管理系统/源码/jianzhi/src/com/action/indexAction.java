package com.action;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.ServletActionContext;

import com.dao.TGonggaoDAO;
import com.dao.TNewsDAO;
import com.dao.TZhiweiDAO;
import com.opensymphony.xwork2.ActionSupport;

public class indexAction extends ActionSupport
{
	private TNewsDAO newsDAO;
	private TZhiweiDAO zhiweiDAO;
	
	
	public TZhiweiDAO getZhiweiDAO()
	{
		return zhiweiDAO;
	}

	public void setZhiweiDAO(TZhiweiDAO zhiweiDAO)
	{
		this.zhiweiDAO = zhiweiDAO;
	}

	public TNewsDAO getNewsDAO()
	{
		return newsDAO;
	}

	public void setNewsDAO(TNewsDAO newsDAO)
	{
		this.newsDAO = newsDAO;
	}

	public String index()
	{
		
		HttpServletRequest request=ServletActionContext.getRequest();
		
		List newsList=newsDAO.findAll();
		if(newsList.size()>4)
		{
			newsList=newsList.subList(0, 4);
		}
		request.setAttribute("newsList", newsList);
		
		
		String sql="from TZhiwei where del='no' order by fabushi desc";
		List zhiweiList=zhiweiDAO.getHibernateTemplate().find(sql);
		if(zhiweiList.size()>6)
		{
			zhiweiList=zhiweiList.subList(0, 6);
		}
		
		request.setAttribute("zhiweiList", zhiweiList);
		
		return ActionSupport.SUCCESS;
	}

}
