package com.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.dao.TGonggaoDAO;
import com.dao.TJianliDAO;
import com.dao.TNewsDAO;
import com.dao.TQiyeDAO;
import com.dao.TZhiweiDAO;
import com.model.TJianli;
import com.model.TNews;
import com.model.TQiye;
import com.model.TUser;
import com.model.TZhiwei;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class jianliAction extends ActionSupport
{
	private Integer id;
	private Integer userId;
	private Integer zhiweiId;
	private String shijian;

	private String fujian;
	private String fujianYuanshiming;
	
	private TJianliDAO jianliDAO;
	private TZhiweiDAO zhiweiDAO;
	
	
	public String jianli_up_user()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		TUser user=(TUser)session.getAttribute("user");
		
		TJianli jianli=new TJianli();
		
		jianli.setUserId(user.getId());
		jianli.setZhiweiId(zhiweiId);
		jianli.setShijian(new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date()));
		jianli.setFujian(fujian);
		
		jianli.setFujianYuanshiming(fujianYuanshiming);
		
		jianliDAO.save(jianli);
		
		request.setAttribute("msg", "����Ͷ�����");
		return "msg";
		
	}
	
	
	public String jianli_mana_user()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		HttpSession session=request.getSession();
		TUser user=(TUser)session.getAttribute("user");
		
		String sql="from TJianli where userId="+user.getId();
		List jianliList=jianliDAO.getHibernateTemplate().find(sql);
		for(int i=0;i<jianliList.size();i++)
		{
			TJianli jianli=(TJianli)jianliList.get(i);
			jianli.setZhiwei(zhiweiDAO.findById(jianli.getZhiweiId()));
		}
		
		request.setAttribute("jianliList", jianliList);
        return ActionSupport.SUCCESS;
	}


	public String getFujian()
	{
		return fujian;
	}


	public void setFujian(String fujian)
	{
		this.fujian = fujian;
	}


	public String getFujianYuanshiming()
	{
		return fujianYuanshiming;
	}


	public void setFujianYuanshiming(String fujianYuanshiming)
	{
		this.fujianYuanshiming = fujianYuanshiming;
	}


	public TZhiweiDAO getZhiweiDAO()
	{
		return zhiweiDAO;
	}


	public void setZhiweiDAO(TZhiweiDAO zhiweiDAO)
	{
		this.zhiweiDAO = zhiweiDAO;
	}


	public Integer getId()
	{
		return id;
	}


	public void setId(Integer id)
	{
		this.id = id;
	}


	public TJianliDAO getJianliDAO()
	{
		return jianliDAO;
	}


	public void setJianliDAO(TJianliDAO jianliDAO)
	{
		this.jianliDAO = jianliDAO;
	}


	public String getShijian()
	{
		return shijian;
	}


	public void setShijian(String shijian)
	{
		this.shijian = shijian;
	}


	public Integer getUserId()
	{
		return userId;
	}


	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}


	public Integer getZhiweiId()
	{
		return zhiweiId;
	}


	public void setZhiweiId(Integer zhiweiId)
	{
		this.zhiweiId = zhiweiId;
	}
	
}
