package com.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.dao.TQiyeDAO;
import com.dao.TUserDAO;
import com.model.TQiye;
import com.model.TUser;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class qiyeAction extends ActionSupport
{
	private Integer id;
	private String loginname;
	private String loginpw;
	private String mingcheng;

	private String dizhi;
	private String dianhua;
	private String youxiang;
	
	private String message;
	private String path;
	
	private TQiyeDAO qiyeDAO;
	
	
	public String qiyeReg()
	{
        HttpServletRequest request=ServletActionContext.getRequest();
		
		String sql="from TQiye where loginname=? and del='no'";
		Object[] c={loginname.trim()};
		List list=qiyeDAO.getHibernateTemplate().find(sql,c);
		if(list.size()>0)
		{
			request.setAttribute("msg", "用户名已被占用,请重新注册");
		}
		else
		{
			TQiye qiye=new TQiye();
			
			qiye.setLoginname(loginname);
			qiye.setLoginpw(loginpw);
			qiye.setMingcheng(mingcheng);
			qiye.setDizhi(dizhi);
			
			qiye.setDianhua(dianhua);
			qiye.setYouxiang(youxiang);
			qiye.setDel("no");
			
			qiyeDAO.save(qiye);
			
			request.setAttribute("msg", "注册成功，请登录");
		}
		
		return "msg";
	}
	
	
	public String qiyeEdit()
	{
		HttpServletRequest request=ServletActionContext.getRequest();
		Map session=ActionContext.getContext().getSession();
		 
        TQiye qiye=(TQiye)session.get("qiye");
			
		qiye.setLoginname(loginname);
		qiye.setLoginpw(loginpw);
		qiye.setMingcheng(mingcheng);
		qiye.setDizhi(dizhi);
		
		qiye.setDianhua(dianhua);
		qiye.setYouxiang(youxiang);
		
		qiyeDAO.attachDirty(qiye);
		
		request.setAttribute("msg", "修改成功，重新登了后生效");
		
		
		return "msg";
	}
	
	
	
	public String qiyeMana()
	{
		String sql="from TQiye where del='no'";
		List qiyeList=qiyeDAO.getHibernateTemplate().find(sql);
		Map request=(Map)ServletActionContext.getContext().get("request");
		request.put("qiyeList", qiyeList);
		return ActionSupport.SUCCESS;
	}
	
	public String qiyeDel()
	{
		TQiye qiye=qiyeDAO.findById(id);
		qiye.setDel("yes");
		qiyeDAO.attachDirty(qiye);
		this.setMessage("删除成功");
		this.setPath("qiyeMana.action");
		return "succeed";
	}



	public String getDianhua()
	{
		return dianhua;
	}



	public void setDianhua(String dianhua)
	{
		this.dianhua = dianhua;
	}



	public String getDizhi()
	{
		return dizhi;
	}



	public void setDizhi(String dizhi)
	{
		this.dizhi = dizhi;
	}



	public Integer getId()
	{
		return id;
	}



	public void setId(Integer id)
	{
		this.id = id;
	}



	public String getLoginname()
	{
		return loginname;
	}



	public void setLoginname(String loginname)
	{
		this.loginname = loginname;
	}



	public String getLoginpw()
	{
		return loginpw;
	}



	public void setLoginpw(String loginpw)
	{
		this.loginpw = loginpw;
	}



	public String getMessage()
	{
		return message;
	}



	public void setMessage(String message)
	{
		this.message = message;
	}



	public String getMingcheng()
	{
		return mingcheng;
	}



	public void setMingcheng(String mingcheng)
	{
		this.mingcheng = mingcheng;
	}



	public String getPath()
	{
		return path;
	}



	public void setPath(String path)
	{
		this.path = path;
	}



	public TQiyeDAO getQiyeDAO()
	{
		return qiyeDAO;
	}



	public void setQiyeDAO(TQiyeDAO qiyeDAO)
	{
		this.qiyeDAO = qiyeDAO;
	}



	public String getYouxiang()
	{
		return youxiang;
	}



	public void setYouxiang(String youxiang)
	{
		this.youxiang = youxiang;
	}
	
}
