package com.dao;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.model.TQiye;

/**
 * Data access object (DAO) for domain model class TQiye.
 * 
 * @see com.model.TQiye
 * @author MyEclipse Persistence Tools
 */

public class TQiyeDAO extends HibernateDaoSupport
{
	private static final Log log = LogFactory.getLog(TQiyeDAO.class);

	protected void initDao()
	{
		// do nothing
	}

	public void save(TQiye transientInstance)
	{
		log.debug("saving TQiye instance");
		try
		{
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re)
		{
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(TQiye persistentInstance)
	{
		log.debug("deleting TQiye instance");
		try
		{
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re)
		{
			log.error("delete failed", re);
			throw re;
		}
	}

	public TQiye findById(java.lang.Integer id)
	{
		log.debug("getting TQiye instance with id: " + id);
		try
		{
			TQiye instance = (TQiye) getHibernateTemplate().get(
					"com.model.TQiye", id);
			return instance;
		} catch (RuntimeException re)
		{
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TQiye instance)
	{
		log.debug("finding TQiye instance by example");
		try
		{
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re)
		{
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value)
	{
		log.debug("finding TQiye instance with property: " + propertyName
				+ ", value: " + value);
		try
		{
			String queryString = "from TQiye as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re)
		{
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findAll()
	{
		log.debug("finding all TQiye instances");
		try
		{
			String queryString = "from TQiye";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re)
		{
			log.error("find all failed", re);
			throw re;
		}
	}

	public TQiye merge(TQiye detachedInstance)
	{
		log.debug("merging TQiye instance");
		try
		{
			TQiye result = (TQiye) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re)
		{
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(TQiye instance)
	{
		log.debug("attaching dirty TQiye instance");
		try
		{
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TQiye instance)
	{
		log.debug("attaching clean TQiye instance");
		try
		{
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public static TQiyeDAO getFromApplicationContext(ApplicationContext ctx)
	{
		return (TQiyeDAO) ctx.getBean("TQiyeDAO");
	}
}