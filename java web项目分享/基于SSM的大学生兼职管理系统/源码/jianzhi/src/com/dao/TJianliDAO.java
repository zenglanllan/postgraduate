package com.dao;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.model.TJianli;

/**
 * Data access object (DAO) for domain model class TJianli.
 * 
 * @see com.model.TJianli
 * @author MyEclipse Persistence Tools
 */

public class TJianliDAO extends HibernateDaoSupport
{
	private static final Log log = LogFactory.getLog(TJianliDAO.class);

	protected void initDao()
	{
		// do nothing
	}

	public void save(TJianli transientInstance)
	{
		log.debug("saving TJianli instance");
		try
		{
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re)
		{
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(TJianli persistentInstance)
	{
		log.debug("deleting TJianli instance");
		try
		{
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re)
		{
			log.error("delete failed", re);
			throw re;
		}
	}

	public TJianli findById(java.lang.Integer id)
	{
		log.debug("getting TJianli instance with id: " + id);
		try
		{
			TJianli instance = (TJianli) getHibernateTemplate().get(
					"com.model.TJianli", id);
			return instance;
		} catch (RuntimeException re)
		{
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TJianli instance)
	{
		log.debug("finding TJianli instance by example");
		try
		{
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re)
		{
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value)
	{
		log.debug("finding TJianli instance with property: " + propertyName
				+ ", value: " + value);
		try
		{
			String queryString = "from TJianli as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re)
		{
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findAll()
	{
		log.debug("finding all TJianli instances");
		try
		{
			String queryString = "from TJianli";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re)
		{
			log.error("find all failed", re);
			throw re;
		}
	}

	public TJianli merge(TJianli detachedInstance)
	{
		log.debug("merging TJianli instance");
		try
		{
			TJianli result = (TJianli) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re)
		{
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(TJianli instance)
	{
		log.debug("attaching dirty TJianli instance");
		try
		{
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TJianli instance)
	{
		log.debug("attaching clean TJianli instance");
		try
		{
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public static TJianliDAO getFromApplicationContext(ApplicationContext ctx)
	{
		return (TJianliDAO) ctx.getBean("TJianliDAO");
	}
}