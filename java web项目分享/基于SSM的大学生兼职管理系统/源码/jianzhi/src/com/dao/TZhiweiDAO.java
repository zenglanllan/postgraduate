package com.dao;

import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.model.TZhiwei;

/**
 * A data access object (DAO) providing persistence and search support for
 * TZhiwei entities. Transaction control of the save(), update() and delete()
 * operations can directly support Spring container-managed transactions or they
 * can be augmented to handle user-managed Spring transactions. Each of these
 * methods provides additional information for how to configure it for the
 * desired type of transaction control.
 * 
 * @see com.model.TZhiwei
 * @author MyEclipse Persistence Tools
 */

public class TZhiweiDAO extends HibernateDaoSupport
{
	private static final Log log = LogFactory.getLog(TZhiweiDAO.class);
	// property constants
	public static final String QIYE_ID = "qiyeId";
	public static final String LEIBIE_ID = "leibieId";
	public static final String MINGCHENG = "mingcheng";
	public static final String XUELI = "xueli";
	public static final String DAIYU = "daiyu";
	public static final String DIDIAN = "didian";
	public static final String JINGYAN = "jingyan";
	public static final String BEIZHU = "beizhu";
	public static final String FABUSHI = "fabushi";
	public static final String DEL = "del";

	protected void initDao()
	{
		// do nothing
	}

	public void save(TZhiwei transientInstance)
	{
		log.debug("saving TZhiwei instance");
		try
		{
			getHibernateTemplate().save(transientInstance);
			log.debug("save successful");
		} catch (RuntimeException re)
		{
			log.error("save failed", re);
			throw re;
		}
	}

	public void delete(TZhiwei persistentInstance)
	{
		log.debug("deleting TZhiwei instance");
		try
		{
			getHibernateTemplate().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re)
		{
			log.error("delete failed", re);
			throw re;
		}
	}

	public TZhiwei findById(java.lang.Integer id)
	{
		log.debug("getting TZhiwei instance with id: " + id);
		try
		{
			TZhiwei instance = (TZhiwei) getHibernateTemplate().get(
					"com.model.TZhiwei", id);
			return instance;
		} catch (RuntimeException re)
		{
			log.error("get failed", re);
			throw re;
		}
	}

	public List findByExample(TZhiwei instance)
	{
		log.debug("finding TZhiwei instance by example");
		try
		{
			List results = getHibernateTemplate().findByExample(instance);
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re)
		{
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List findByProperty(String propertyName, Object value)
	{
		log.debug("finding TZhiwei instance with property: " + propertyName
				+ ", value: " + value);
		try
		{
			String queryString = "from TZhiwei as model where model."
					+ propertyName + "= ?";
			return getHibernateTemplate().find(queryString, value);
		} catch (RuntimeException re)
		{
			log.error("find by property name failed", re);
			throw re;
		}
	}

	public List findByQiyeId(Object qiyeId)
	{
		return findByProperty(QIYE_ID, qiyeId);
	}

	public List findByLeibieId(Object leibieId)
	{
		return findByProperty(LEIBIE_ID, leibieId);
	}

	public List findByMingcheng(Object mingcheng)
	{
		return findByProperty(MINGCHENG, mingcheng);
	}

	public List findByXueli(Object xueli)
	{
		return findByProperty(XUELI, xueli);
	}

	public List findByDaiyu(Object daiyu)
	{
		return findByProperty(DAIYU, daiyu);
	}

	public List findByDidian(Object didian)
	{
		return findByProperty(DIDIAN, didian);
	}

	public List findByJingyan(Object jingyan)
	{
		return findByProperty(JINGYAN, jingyan);
	}

	public List findByBeizhu(Object beizhu)
	{
		return findByProperty(BEIZHU, beizhu);
	}

	public List findByFabushi(Object fabushi)
	{
		return findByProperty(FABUSHI, fabushi);
	}

	public List findByDel(Object del)
	{
		return findByProperty(DEL, del);
	}

	public List findAll()
	{
		log.debug("finding all TZhiwei instances");
		try
		{
			String queryString = "from TZhiwei";
			return getHibernateTemplate().find(queryString);
		} catch (RuntimeException re)
		{
			log.error("find all failed", re);
			throw re;
		}
	}

	public TZhiwei merge(TZhiwei detachedInstance)
	{
		log.debug("merging TZhiwei instance");
		try
		{
			TZhiwei result = (TZhiwei) getHibernateTemplate().merge(
					detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re)
		{
			log.error("merge failed", re);
			throw re;
		}
	}

	public void attachDirty(TZhiwei instance)
	{
		log.debug("attaching dirty TZhiwei instance");
		try
		{
			getHibernateTemplate().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(TZhiwei instance)
	{
		log.debug("attaching clean TZhiwei instance");
		try
		{
			getHibernateTemplate().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re)
		{
			log.error("attach failed", re);
			throw re;
		}
	}

	public static TZhiweiDAO getFromApplicationContext(ApplicationContext ctx)
	{
		return (TZhiweiDAO) ctx.getBean("TZhiweiDAO");
	}
}