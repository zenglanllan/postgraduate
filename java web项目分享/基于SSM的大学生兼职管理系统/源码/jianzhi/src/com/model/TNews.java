package com.model;


public class TNews implements java.io.Serializable
{

	private Integer newsId;
	private String newsTitle;
	private String newsContent;
	private String fujian;
	
	private String fujianYuanshiming;
	private String newsDate;

	public String getNewsContent()
	{
		return newsContent;
	}

	public void setNewsContent(String newsContent)
	{
		this.newsContent = newsContent;
	}

	public String getNewsDate()
	{
		return newsDate;
	}

	public void setNewsDate(String newsDate)
	{
		this.newsDate = newsDate;
	}

	public Integer getNewsId()
	{
		return newsId;
	}

	public void setNewsId(Integer newsId)
	{
		this.newsId = newsId;
	}

	public String getFujian()
	{
		return fujian;
	}

	public void setFujian(String fujian)
	{
		this.fujian = fujian;
	}

	public String getFujianYuanshiming()
	{
		return fujianYuanshiming;
	}

	public void setFujianYuanshiming(String fujianYuanshiming)
	{
		this.fujianYuanshiming = fujianYuanshiming;
	}

	public String getNewsTitle()
	{
		return newsTitle;
	}

	public void setNewsTitle(String newsTitle)
	{
		this.newsTitle = newsTitle;
	}

}