<%@ page language="java" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title></title>
    <base target="_self">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
    <link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
    
    <script type="text/javascript">
    	function next()
    	{
    		var type = document.formAdd.type.value;
    		
    		if(type == 1)
    		{
    			document.formAdd.action="<%=path %>/site/zhuce/zhuce_qiye.jsp";
    		}
    		if(type == 2)
    		{
    			document.formAdd.action="<%=path %>/site/zhuce/zhuce_user.jsp";
    		}
    		
    		document.formAdd.submit();
    	}
    	
    </script>

  </head>
  
	<body leftmargin="2" topmargin="9" background='<%=path %>/img/allbg.gif'>
		<form action="" method="post" name="formAdd">
				 <table width="98%" align="center" border="0" cellpadding="4" cellspacing="1" bgcolor="#CBD8AC" style="margin-bottom:8px">
					    <tr bgcolor="#EEF4EA">
					        <td colspan="3" background="<%=path %>/img/wbg.gif" class='title'><span>&nbsp;</span></td>
					    </tr>
						<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						          注册类型：
						    </td>
						    <td width="75%" bgcolor="#FFFFFF" align="left">
						        <select name="type" style="width: 150px;">
									<option value="1">用人单位</option>	
									<option value="2">普通用户</option>	
								</select> 
						    </td>
						</tr>
						<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
						    <td width="25%" bgcolor="#FFFFFF" align="right">
						        &nbsp;
						    </td>
						    <td width="75%" bgcolor="#FFFFFF" align="left">
						       <input type="button" value="下一步" onclick="next()"/>&nbsp; 
						    </td>
						</tr>							
				 </table>
		</form>				 				 
  </body>
</html>
