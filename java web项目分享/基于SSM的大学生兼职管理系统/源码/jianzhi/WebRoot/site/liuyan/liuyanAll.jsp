<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<style type="text/css">
	    .c1-bline{border-bottom:#999 1px dashed;border-top:1px;}
		.f-right{float:right}
		.f-left{float:left}
		.clear{clear:both}
	</style>
	
    <script language="javascript">
		
		
		function liuyanAdd()
        {
            <c:if test="${sessionScope.userType!=1}">
                  alert("请先登录");
            </c:if>
            <c:if test="${sessionScope.userType==1}">
                 var strUrl = "<%=path %>/site/liuyan/liuyanAdd.jsp";
	             var ret = window.showModalDialog(strUrl,"","dialogWidth:800px; dialogHeight:500px; dialogLeft: status:no; directories:yes;scrollbars:yes;Resizable=no;");
	             window.location.reload();
            </c:if>
        }
        
        function liuyanDetail(id)
        {
             var strUrl = "<%=path %>/liuyanDetail.action?id="+id;
             var ret = window.showModalDialog(strUrl,"","dialogWidth:800px; dialogHeight:500px; dialogLeft: status:no; directories:yes;scrollbars:yes;Resizable=no;");
        }
     </script>
  </head>
  
  <body>
	
	<div id="container">
    	<jsp:include flush="true" page="/site/inc/head.jsp"></jsp:include>
		
		
		
		
		
		
		
        <div class="cls10" style="height: 1px;"></div>
       	<div id="newest_box">
           	<div class="hd">
                   <div class="title">
                       <span style="float:left">网站留言板</span>
					   <span style="float:right"><a href="#" onclick="liuyanAdd()">我要留言</a>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                   </div>
           	</div>
            <div style="height: 400px;">
               	<div class="c1-body">
                     <c:forEach items="${requestScope.liuyanList}" var="liuyan" varStatus="sta">
				           <div class="c1-bline" style="padding:7px 0px;">
		                        <div class="f-left">
		                             &nbsp;&nbsp;&nbsp;<img src="<%=path %>/img/head-mark4.gif" align="middle" class="img-vm" border="0"/> 
		                             <a href="#" onclick="liuyanDetail(${liuyan.id })">${liuyan.neirong }</a>
		                        </div>
					            <div class="f-right">${liuyan.liuyanshi }&nbsp;&nbsp;&nbsp;</div>
					            <div class="clear"></div>
				           </div>             
				     </c:forEach>
					 <div class="pg-3"></div>             		  
				 </div>
            </div>
        </div>
            
            
            
                  
          
                   
            
        
        
        <jsp:include flush="true" page="/site/inc/foot.jsp"></jsp:include>
    </div>
</body>
</html>
