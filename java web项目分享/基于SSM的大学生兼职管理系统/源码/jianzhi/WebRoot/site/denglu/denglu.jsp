<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<script type="text/javascript" src="/wuye/js/jquery.min.js"></script>
	
	<style type="text/css">
	</style>
	
	<script type='text/javascript' src='<%=path %>/dwr/interface/loginService.js'></script>
    <script type='text/javascript' src='<%=path %>/dwr/engine.js'></script>
    <script type='text/javascript' src='<%=path %>/dwr/util.js'></script>
	
	<script language="javascript">
		function login()
	        {
	           if(document.getElementById("loginname").value=="")
	           {
	               alert("请输入用户名");
	               return;
	           }
	           if(document.getElementById("loginpw").value=="")
	           {
	               alert("请输入密码");
	               return;
	           }
	           
	           document.getElementById("indicator").style.display="block";
			   loginService.login(document.getElementById("loginname").value,document.getElementById("loginpw").value,2,callback);
	        }
	        
	        
	        function callback(data)
			{
			    document.getElementById("indicator").style.display="none";
			    if(data=="no")
			    {
			        alert("用户名或密码错误");
			    }
			    if(data=="yes")
			    {
			        alert("通过验证,系统登录成功");
			        var url="<%=path %>/index.action"
			        window.location.href=url;
			    }
			    
			}
    </script>
  </head>
  
  <body>
	
	<div id="container">
    	<jsp:include flush="true" page="/site/inc/head.jsp"></jsp:include>
		
		
		
		
            
        <div class="cls10" style="height: 1px;"></div>
       	<div id="newest_box">
           	<div class="hd">
                   <div class="title">
                       <a href="#">用户登录</a>
                   </div>
           	</div>
            <div class="news_content_box content_box">
                   <br/>
               	   <FORM name="ThisForm" method="post" action=""> 
				   <TABLE align="center">
				        <tr align='center'>
							<td style="width: 60px;" align="left">
								账&nbsp;&nbsp;&nbsp;&nbsp;号：									    
							</td>
							<td align="left">
								<input type="text" name="loginname" id="loginname" style="width: 300px;"/>
							</td>
						</tr>
						<tr align='center'>
							<td style="width: 60px;" align="left">
								密&nbsp;&nbsp;&nbsp;&nbsp;码：										    
							</td>
							<td align="left">
								<input type="password" name="loginpw" id="loginpw" style="width: 300px;"/>
							</td>
						</tr>
						<tr align='center'>
						   <td style="width: 60px;" align="left"></td>
						   <td align="left">
						      <input type="button" value="登录" style="width: 70px;" onclick="login()"/>&nbsp; 
						      <input type="reset" value="重置" style="width: 70px;"/>&nbsp;	
						      <img id="indicator" src="<%=path %>/img/loading.gif" style="display:none"/>
						   </td>
						</tr>
				   </TABLE>
			   </FORM>
            </div>
        </div>
        
           
        
        
        
        <jsp:include flush="true" page="/site/inc/foot.jsp"></jsp:include>
    </div>
</body>
</html>
