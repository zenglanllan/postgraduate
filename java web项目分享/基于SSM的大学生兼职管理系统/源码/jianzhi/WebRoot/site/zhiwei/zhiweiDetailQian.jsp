<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<style type="text/css">
	</style>
	
	
    <script language="JavaScript" type="text/javascript">
         function jianli_up_user(zhiweiId)
         {
            <s:if test="#session.userType !=2">
	             alert("请先登录");
	        </s:if>
	        <s:if test="#session.userType==2">
	             var strUrl = "<%=path %>/site/jianli/jianli_up_user.jsp?zhiweiId="+zhiweiId;
                 var ret = window.showModalDialog(strUrl,"","dialogWidth:700px; dialogHeight:450px; dialogLeft: status:no; directories:yes;scrollbars:yes;Resizable=no;");
	        </s:if>
         }
    </script>
  </head>
  
  <body>
	
	<div id="container">
    	<jsp:include flush="true" page="/site/inc/head.jsp"></jsp:include>
		
		
		
		
            
        <div class="cls10" style="height: 1px;"></div>
       	<div id="newest_box">
           	<div class="hd">
                   <div class="title">
                       <a href="#">信息详情</a>
                   </div>
           	</div>
            <div style="margin-left: 12px;margin-right: 12px;">
                 <table width="100%" border="0" cellpadding="5" cellspacing="5">
						    <tr>
						       <td align="left">职位名称：${requestScope.zhiwei.mingcheng }</td>
						    </tr>
						    <tr>
						       <td align="left">学历要求：${requestScope.zhiwei.xueli }</td>
						    </tr>
						    <tr>
						       <td align="left">工资待遇：${requestScope.zhiwei.daiyu }</td>
						    </tr>
						    <tr>
						       <td align="left">工作地点：${requestScope.zhiwei.didian }</td>
						    </tr>
						    <tr>
						       <td align="left">工作经验：${requestScope.zhiwei.jingyan }</td>
						    </tr>
						    <tr>
						       <td align="left">备注信息：${requestScope.zhiwei.beizhu }</td>
						    </tr>
						    <tr>
						       <td align="left">招聘单位：${requestScope.zhiwei.qiye.mingcheng }</td>
						    </tr>
						    <tr>
						       <td align="left">发布时间：${requestScope.zhiwei.fabushi }</td>
						    </tr>
						    <tr>
						       <td align="left"><input type="button" value="投递简历" style="width: 100px;" onclick="jianli_up_user(${requestScope.zhiwei.id })"/></td>
						    </tr>
                        </table>
            </div>
        </div>
        
           
        
        
        
        <jsp:include flush="true" page="/site/inc/foot.jsp"></jsp:include>
    </div>
</body>
</html>
