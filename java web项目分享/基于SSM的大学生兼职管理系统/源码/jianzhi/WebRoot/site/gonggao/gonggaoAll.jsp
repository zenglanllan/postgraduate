<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
	<meta http-equiv="pragma" content="no-cache"/>
	<meta http-equiv="cache-control" content="no-cache"/>
	<meta http-equiv="expires" content="0"/>    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3"/>
	<meta http-equiv="description" content="This is my page"/>
	
	<script type="text/javascript" src="/wuye/js/jquery.min.js"></script>
	
	<style type="text/css">
		.content_box{width:100%;}
		.content_box ul{padding:0px;margin:0px;padding-left:30px;margin-top:15px;font-size:14px;padding-right:10px;}
		.content_box ul li{ margin-bottom:5px;padding:0px;}
		.content_box ul li a{ text-decoration:none;color:#6e572a;}
		.content_box ul li a:hover{text-decoration:underline;}
		.content_box ul li span{float: right;font-size: 12px;color: #b9ad9e;height: 20px;line-height: 20px;text-align: center;font-family: Arial, Helvetica, sans-serif;}
	</style>
	
	<script language="javascript">
     </script>
  </head>
  
  <body>
	
	<div id="container">
    	<jsp:include flush="true" page="/site/inc/head.jsp"></jsp:include>
		
		
		
		
		
		
		
        <div class="cls10" style="height: 1px;"></div>
       	<div id="newest_box">
           	<div class="hd">
                   <div class="title">
                       <a href="#">公告信息</a>
                   </div>
           	</div>
            <div class="news_content_box content_box" style="height: 400px;">
               	<ul>
                   	<c:forEach items="${requestScope.gonggaoList}" var="gonggao" varStatus="ss">
                    <li><span>${gonggao.gonggaoData }</span><a href="<%=path %>/gonggaoDetailQian.action?gonggaoId=${gonggao.gonggaoId }">${gonggao.gonggaoTitle }</a></li>
                    </c:forEach>
                </ul>
            </div>
        </div>
            
            
            
                  
          
                   
            
        
        
        <jsp:include flush="true" page="/site/inc/foot.jsp"></jsp:include>
    </div>
</body>
</html>
