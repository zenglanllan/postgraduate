<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
String path = request.getContextPath();
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="pragma" content="no-cache" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3" />
		<meta http-equiv="description" content="This is my page" />

		<link rel="stylesheet" type="text/css" href="<%=path %>/css/base.css" />
		
		<script language="JavaScript" src="<%=path %>/js/public.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%=path %>/js/popup.js"></script>
        <script language="javascript">
           function zhiwei_del(id)
           {
               if(confirm('您确定删除吗？'))
               {
                   window.location.href="<%=path %>/zhiwei_del.action?id="+id;
               }
           } 
           
            function zhiwei_jianli(zhiweiId)
		    {
		        var url="<%=path %>/zhiwei_jianli.action?zhiweiId="+zhiweiId;
		        window.location.href=url;
		    }          
       </script>
	</head>

	<body leftmargin="2" topmargin="2" background='<%=path %>/img/allbg.gif'>
			<table width="98%" border="0" cellpadding="2" cellspacing="1" bgcolor="#D1DDAA" align="center" style="margin-top:8px">
				<tr bgcolor="#E7E7E7">
					<td height="14" colspan="9" background="<%=path %>/img/tbg.gif">&nbsp;</td>
				</tr>
				<tr align="center" bgcolor="#FAFAF1" height="22">
				    <td width="5%">序号</td>
				    
					<td width="20%">职位名称</td>
					<td width="10%">学历要求</td>
					<td width="10%">工资待遇</td>
					<td width="10%">工作地点</td>
					
					<td width="10%">工作经验</td>
					<td width="10%">备注信息</td>
					<td width="10%">发布时间</td>
					<td width="10%">操作</td>
		        </tr>	
				<s:iterator value="#request.zhiweiList" id="zhiwei" status="ss">
				<tr align='center' bgcolor="#FFFFFF" onMouseMove="javascript:this.bgColor='red';" onMouseOut="javascript:this.bgColor='#FFFFFF';" height="22">
					<td bgcolor="#FFFFFF" align="center">
						 <s:property value="#ss.index+1"/>
					</td>
					
					<td bgcolor="#FFFFFF" align="center">
						 <s:property value="#zhiwei.mingcheng"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
					   <s:property value="#zhiwei.xueli"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						<s:property value="#zhiwei.daiyu"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						 <s:property value="#zhiwei.didian"/>
					</td>
					
					<td bgcolor="#FFFFFF" align="center">
						<s:property value="#zhiwei.jingyan"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    <s:property value="#zhiwei.beizhu" escape="false"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
					    <s:property value="#zhiwei.fabushi"/>
					</td>
					<td bgcolor="#FFFFFF" align="center">
						<a href="#" style="color: red" onclick="zhiwei_del(<s:property value="#zhiwei.id"/>)">删除</a>
						<a href="#" style="color: red" onclick="zhiwei_jianli(<s:property value="#zhiwei.id"/>)">查看应聘信息</a>
					</td>
				</tr>
				</s:iterator>
			</table>
	</body>
</html>
