<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page isELIgnored="false" %> 
<%
String path = request.getContextPath();
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
    <LINK href="<%=path %>/css/admin.css" type="text/css" rel="stylesheet">
	<SCRIPT language=javascript>
		function expand(el)
		{
			childObj = document.getElementById("child" + el);
	
			if (childObj.style.display == 'none')
			{
				childObj.style.display = 'block';
			}
			else
			{
				childObj.style.display = 'none';
			}
			return;
		}
	</SCRIPT>
  </head>
  
  <BODY>
        <c:if test="${sessionScope.userType==0}">
		<TABLE height="100%" cellSpacing=0 cellPadding=0 width=170 background=<%=path %>/img/menu_bg.jpg border=0>
			<TR>
				<TD vAlign=top align=middle>
					<TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
						<TR><TD height=13></TD></TR>
					</TABLE>
					
					
					
					
					
					
					<!-- 第一个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(1) href="javascript:void(0);">修改登录密码</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child1 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/admin/userinfo/userPw.jsp" target=main>修改登录密码</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第一个菜单项 -->
					
					
					
					
					<!-- 第一个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(11) href="javascript:void(0);">信息类别管理</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child11 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/leibieMana.action" target=main>信息类别管理</A>
							</TD>
						</TR>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/admin/leibie/leibieAdd.jsp" target=main>添加信息类别</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第一个菜单项 -->
					
					
					<!-- 第一个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(2) href="javascript:void(0);">用人单位管理</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child2 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/qiyeMana.action" target=main>用人单位管理</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第一个菜单项 -->
					
					
					
					<!-- 第一个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(3) href="javascript:void(0);">普通用户管理</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child3 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/userMana.action" target=main>普通用户管理</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第一个菜单项 -->
					
					
					
					
					
					
					
					<!-- 第二个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(5) href="javascript:void(0);">系统公告管理</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child5 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/gonggaoMana.action" target=main>系统公告管理</A>
							</TD>
						</TR>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/admin/gonggao/gonggaoAdd.jsp" target=main>系统公告添加</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第二个菜单项 -->
					
					
					
					
					<!-- 第二个菜单项 -->
					<TABLE cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=22>
							<TD style="PADDING-LEFT: 30px" background="<%=path %>/img/menu_bt.jpg">
								<A class=menuParent onclick=expand(6) href="javascript:void(0);">新闻资讯管理</A>
							</TD>
						</TR>
						<TR height=4>
							<TD></TD>
						</TR>
					</TABLE>
					<TABLE id=child6 style="DISPLAY: none" cellSpacing=0 cellPadding=0 width=150 border=0>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/newsMana.action" target=main>新闻资讯管理</A>
							</TD>
						</TR>
						<TR height=20>
							<TD align=middle width=30>
								<IMG height=9 src="<%=path %>/img/menu_icon.gif" width=9>
							</TD>
							<TD>
								<A class=menuChild href="<%=path %>/admin/news/newsAdd.jsp" target=main>新闻资讯添加</A>
							</TD>
						</TR>
					</TABLE>
					<!-- 第二个菜单项 -->
					
					
					
					
				
					
					
				</TD>
				<TD width=1 bgColor=#d1e6f7></TD>
			</TR>
		</TABLE>
		</c:if>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	</BODY>
</html>
